/**
 * Created by Damien on 23/10/2015.
 */
(function(){
    var selectionVideo = angular.module("selectVid", ['ngRoute']);

    selectionVideo.config(["$routeProvider", function($routeProvider){
        $routeProvider.when("/home",{
            templateUrl:"/playeur.html",
            controller:"selectCtrl"
        }).otherwise({
            redirectTo:"/home"
        })
    }]);

    /*.when("/upload", {
        templateUrl:"/upload/upload.html",
        controller:"uploadCtrl"
    })*/

    selectionVideo.controller("selectCtrl",["$log", "$http", "$scope",function($log, $http,$scope){
        $scope.videos = [];
        $scope.qualitySelect= 0;
        $scope.context = new Dash.di.DashContext();
        $scope.player = new MediaPlayer($scope.context);
        $("#upload").removeClass("active");
        $("#home").addClass("active");

        $scope.initVideo = function(url){
            if(url == null){
                url = "/yowamyshi.mpd";
            }/*
            $scope.context = new Dash.di.DashContext();
            $scope.player = new MediaPlayer($scope.context);*/
            $scope.player.startup();
            $scope.player.attachView(document.querySelector("#videoPlayer"));
            $scope.player.attachSource(url);
        };


        $scope.loadVideo = function (path,mdp, event) {
            var mdpLink = $("#manifest")[0];
            mdpLink.src = "/"+mdp;

            var videoLink = $("#video")[0];
            videoLink.src = "/"+path;
            var video = $("video")[0];
            video.load();
            $scope.qualitySelect = 0;
            //$scope.initVideo("/movie/"+mdp);
            event.stopPropagation();
            event.preventDefault();
        };

        //$scope.initVideo(null);

        $http.get("/video", {responseType:'json'}).success(function(data){
            $scope.videos = data;
        }).error(function(data, status){
            $log.error(data+"|"+"request failed");
            $log.error(status);
        });

        $scope.changeQuality = function(){
            var video = "";

            if($scope.qualitySelect==0){
                video = "/yowamyshi.webm"
            }else {
                video = "/yowamushi" + quality[$scope.qualitySelect];
            }
            var playeur = $("video")[0];
            var currentTime = playeur.currentTime;
            $log.info(video);
            var videoLink = $("#video")[0];
            videoLink.src = video;

            playeur.load();
            playeur.currentTime = currentTime;
            playeur.play();
            /*var bitrates = $scope.player.getBitrateInfoListFor("video");
            switch($scope.qualitySelect){
                case 480:
                    $scope.player.setQualityFor("video", bitrates[bitrates.length-1].qualityIndex);
                    break;
                case 540:
                    $scope.player.setQualityFor("video", bitrates[1].qualityIndex);
                    break;
                case 720:
                    $scope.player.setQualityFor("video", 0);
                    break;
                default:

            }*/
        }

    }]);

    selectionVideo.controller("uploadCtrl", ["$log", function($log){
        $("#home").removeClass("active");
        $("#upload").addClass("active");
    }]);
})();


var quality = {480:"50kbps.mp4", 540:"100kbps.mp4", 720:"250kbps.mp4"};