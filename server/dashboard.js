/**
 * Created by d.bidaud on 16/10/2015.
 */

exports.mediaTypes = [
    {contentType:"application/dash+xml"}
];

exports.create = function(mediaType){
  var mediaFactory = {
      'application/dash+xml' : createDashboardMedia(mediaType.format)
  };
    var media = mediaFactory[mediaType.contentType];
    return media;
};

function createDashboardMedia(format){
    return function mediaForma() {
        var formats = {
            'json': '{ "account": { "href":  "/account" }, "products": { "href": "/products" } }',
            'xml': '<MPD>' +
            '<Period>' +
            '<AdaptationSet>' +
            '<Representation>' +
            '<BaseURL></BaseURL>' +
            '<SegmentBase>' +
            '<Initialization/>' +
            '</SegmentBase>' +
            '</Representation>' +
            '</AdaptationSet>' +
            '</Period>' +
            '</MPD>'
        };

        return formats[format];
    }
}