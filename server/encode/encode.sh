#!/bin/bash

#fragment
mp4fragment source file newFile

#convert
ffmpeg -i "source file" -codec:v libvpx -quality good -cpu-used 0 -b:v 500k -r 25 -qmin 10 -qmax 42 -maxrate 800k -bufsize 1600k -threads 4 -vf scale=-1:360 -codec:a libvorbis -b:a 128k -pass 2 -f webm "newFile".webm

#480
ffmpeg  -i "fichier source" -codec:v libvpx -quality good -cpu-used 0 -b:v 600k -maxrate 600k -bufsize 1200k -qmin 10 -qmax 42 -vf scale=-1:480 -threads 4 -codec:a libvorbis -b:a 128k sortie_480p.webm

#576
ffmpeg -i "fichier source" -codec:v libvpx -quality good -cpu-used 0 -b:v 1000k -maxrate 1000k -bufsize 2000k -qmin 10 -qmax 42 -vf scale=-1:576 -threads 4 -codec:a libvorbis -b:a 128k sortie_576p.webm

#720p
ffmpeg -i "fichier source" -codec:v libvpx -quality good -cpu-used 0 -b:v 2000k -maxrate 2000k -bufsize 4000k -qmin 10 -qmax 42 -vf scale=-1:720 -threads 4 -codec:a libvorbis -b:a 128k sortie_720p.webm

#1080
ffmpeg -i "fichier source" -codec:v libvpx -quality good -cpu-used 0 -b:v 4000k -maxrate 4000k -bufsize 8000k -qmin 10 -qmax 42 -vf scale=-1:1080 -threads 4 -codec:a libvorbis -b:a 128k sortie_1080p.webm

#audio
ffmpeg -i mon_fichier_principal.webm -vn -acodec libvorbis -ab 128k mon_fichier_audio.webm

#aligner les flux
samplemuxer -i ma_video-480kbps.webm -o ma_video-480kbps-finale.web
samplemuxer -i ma_video-576kbps.webm -o ma_video-576kbps-finale.web
samplemuxer -i ma_video-720kbps.webm -o ma_video-720kbps-finale.web
samplemuxer -i ma_video-1080kbps.webm -o ma_video-1080kbps-finale.web

samplemuxer -i mavideo.webm -o mon_audio-final.webm -output_cues 1 -cues_on_audio_track 1 -max_cluster_duration 2 -audio_track_number

#creat manifest
../webm-tools/webm_dash_manifest/webm_dash_manifest -o yowamyshi.mpd -as id=0,lang=eng -r id=0,file=sortie_480p-finale.webm -r id=1,file=sortie_576p-finale.webm -r id=2,file=sortie_720p-finale.webm -r id=3,file=sortie_1080p-finale.webm -as id=1,lang=eng -r id=5,file=mon_audio-final.webm



ffmpeg -f webm_dash_manifest -i sortie_480p-finale.webm -f webm_dash_manifest -i sortie_576p-finale.webm -f webm_dash_manifest -i sortie_720p-finale.webm -f webm_dash_manifest -i sortie_1080p-finale.webm -f webm_dash_manifest -i mon_audio-final.webm -map 0 -map 1 -map 2 -map 3 -map 4 -c copy -f webm_dash_manifest -adaptation_sets "id=0,streams=0,3 id=1,streams=4" yowamyshi.mpd