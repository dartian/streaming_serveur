/**
 * Created by Damien on 23/10/2015.
 */

var mysql = require("mysql");
var Q = require("q");

var connection = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"dbydtqp91",
    database:"streamingserver"
});
module.exports  = connection;

module.exports.findAll = function (sql, params) {
    var deferred = Q.defer();
    connection.query(sql, params, function (err, res) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(res);
        }
    });
    return deferred.promise;
};