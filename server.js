/**
 * Created by Damien on 30/09/2015.
 */
var http = require("http");
var express = require("express");
var leisure = require("leisure");
var accept = leisure.accept;
var path = require("path");
var send = require("send");
var dashboard = require("./server/dashboard");
var app = express();
var connection = require("./server/db/database");
var fs = require("fs");
var mime = require("mime-types");
var url = require("url");

function range(req, res, next){
    var files = req.url.split(".");
    if (files[1] == "mpd") {
        console.log("mpd");
        mime.extension("application/dash+xml");
        mime.lookup(".mpd");
        //var media = dashboard.create("/movie/"+req.url);
        var file = req.url;
        res.sendFile(path.join(__dirname,"/movie", file));
    } else {
        var file = path.resolve(path.join(__dirname,"/movie", req.url));
        var range = req.headers.range;
        var positions = range.replace(/bytes=/, "").split("-");
        var start = parseInt(positions[0], 10);

        fs.stat(file, function(err, stats) {
            var total = stats.size;
            var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
            var chunksize = (end - start) + 1;

            res.writeHead(206, {
                "Content-Range": "bytes " + start + "-" + end + "/" + total,
                "Accept-Ranges": "bytes",
                "Content-Length": chunksize,
                "Content-Type": "video/mp4"
            });
            console.log(start+"-"+end);
            var stream = fs.createReadStream(file, { start: start, end: end })
                .on("open", function() {
                    stream.pipe(res);
                }).on("error", function(err) {
                    res.end(err);
                });
        });
    }
}

app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "client")));
app.use(express.static(path.join(__dirname,"/movie")));


var server = app.listen(80, function(){
    var host = server.address().address;
    var port = server.address().port;
    mime.extension("application/octet-stream");
    mime.lookup(".mpd");
    console.log("'Server running at "+host+" port:"+port);
});



app.get("/", accept(dashboard.mediaTypes), function(req, res){
    var media = dashboard.create(req.accepted);
    res.send(media);
    res.sendFile("index.html");
});

app.get("/upload", function(req, res){
    res.sendFile(__dirname+"/public/upload/upload.html");
});

app.get("/video", function(req, res){
    connection.findAll("SELECT * FROM video").then(function(videos){
        res.send(res.json(videos));
    });
});
